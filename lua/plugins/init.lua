return require('packer').startup(function(use)
  use 'wbthomason/packer.nvim'
  use 'rockerBOO/boo-colorscheme-nvim'
  use 'nvim-lua/plenary.nvim'
  use "lukas-reineke/indent-blankline.nvim"
  use {'jose-elias-alvarez/null-ls.nvim', config = "require('null-ls-config')", requires = { "nvim-lua/plenary.nvim"}}
  use 'folke/lsp-colors.nvim'
  use "folke/which-key.nvim"
  use 'kyazdani42/nvim-web-devicons'
  use {
  "folke/trouble.nvim",
  requires = "kyazdani42/nvim-web-devicons"}
  use {'windwp/nvim-autopairs', config = "require('autopairs-config')", after = "nvim-cmp"}
  use {
        'nvim-treesitter/nvim-treesitter',
        run = ':TSUpdate',
        event = "BufWinEnter",
        config = "require('treesitter-config')"
    }
  use {
        'akinsho/bufferline.nvim', 
        requires = 'kyazdani42/nvim-web-devicons',
	}
  use {
      'kyazdani42/nvim-tree.lua',
      requires = {
      'kyazdani42/nvim-web-devicons',
      cmd = "NvimTreeToggle",
      config = "require('nvim-tree-config')"
    },
      config = function() require'nvim-tree'.setup {} end,
  }
  use {
    'nvim-telescope/telescope.nvim',
    requires = {{'nvim-lua/plenary.nvim'}},
    cmd = "Telescope",
    config = "require('telescope-config')"
  }
  use {'neovim/nvim-lspconfig', config = "require('lsp')"}
  use {'hrsh7th/cmp-nvim-lsp'}
  use {'hrsh7th/cmp-buffer'}
  use {'hrsh7th/nvim-cmp'}
  use {'hrsh7th/cmp-vsnip'}
  use {'hrsh7th/vim-vsnip'}
  use {'williamboman/nvim-lsp-installer'}
  use 'glepnir/dashboard-nvim'
  use 'ray-x/go.nvim'
  use {"folke/zen-mode.nvim", config = 'require("zen-mode-config")'}
  use {"folke/twilight.nvim", config = "require('twilight-config')"}
  use {
    'hoob3rt/lualine.nvim',
    requires = {'kyazdani42/nvim-web-devicons', opt = true}}
  use "tpope/vim-surround"
  use "tpope/vim-fugitive"
  use "tpope/vim-repeat"
  use "tpope/vim-eunuch"
  use "tpope/vim-unimpaired"
  use "tpope/vim-abolish"
  use "cljoly/telescope-repo.nvim"
  use { "nvim-telescope/telescope-file-browser.nvim" }
  use { "nvim-telescope/telescope-ui-select.nvim" }
  use "dhruvmanila/telescope-bookmarks.nvim"
  use "nvim-telescope/telescope-github.nvim"
  use {
      "AckslD/nvim-neoclip.lua",
      config = function()
        require("neoclip").setup()
      end,
    }
  use { "nvim-telescope/telescope-fzf-native.nvim", run = "make" }
  use "andymass/vim-matchup"
  use "nvim-lua/lsp_extensions.nvim"
  use "nvim-lua/popup.nvim"
  use "nvim-lua/lsp-status.nvim"
  use "folke/lua-dev.nvim"
  use "onsails/lspkind-nvim"
  use "ray-x/lsp_signature.nvim"
end)
