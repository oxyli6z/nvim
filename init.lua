require('plugins')
require('options')
require('lualine-config')
require('dashboard-config')
require('blankline-config')
require('bufferline-config')
require('trouble-config')
vim.cmd([[colorscheme boo]])

